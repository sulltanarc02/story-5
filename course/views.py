from django.shortcuts import render, get_object_or_404, redirect
from .models import Course
from .form import FormCourse
from django.http import HttpResponseRedirect

# Create your views here.
def home(request):
    course = Course.objects.all()
    return render(request, 'course/home.html', {'course':course})

def detail(request, pk):
    kuliah = Course.objects.get(pk=pk) 
    context = {
        "detail" : kuliah,
    }
    return render(request, "course/detail.html", context)

def tambah(request):
    form = FormCourse()

    if request.method == "POST":
        form = FormCourse(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/course/")

    kuliah = Course.objects.all()

    context = {
        "form" : form,
        "listCourse" : kuliah,
    }

    return render(request, "course/tambah.html", context)
    course = Course.objects.all()

def hapus(request, pk):
    kelas = Course.objects.filter(pk=pk)
    kelas.delete()    
    return HttpResponseRedirect('/course/')
