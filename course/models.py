from django.db import models

# Create your models here.
class Course(models.Model):
    nama = models.CharField(max_length=100)
    dosen = models.CharField(max_length=100)
    sks = models.CharField(max_length= 20)
    tahun = models.CharField(max_length=100)
    ruang = models.CharField(max_length=100)
    deskripsi = models.TextField()

