from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'course'

urlpatterns = [
    path('', views.home, name='home'),
    path('tambah/', views.tambah),
    path('detail/<int:pk>', views.detail, name = 'detail'),
    path('hapus/<int:pk>', views.hapus, name = 'hapus')
]
