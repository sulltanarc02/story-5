from django import forms
from .models import Course

class FormCourse(forms.ModelForm) :
    class Meta:
        model = Course
        fields = [
            'nama',
            'dosen',
            'sks',
            'tahun',
            'ruang',
            'deskripsi',
        ]

        widgets = {
            'nama' : forms.TextInput(attrs={'class' : 'form-control'}),
            'dosen' : forms.TextInput(attrs={'class' : 'form-control'}),
            'sks' : forms.NumberInput(attrs={'class' : 'form-control'}),
            'tahun' : forms.TextInput(attrs={'class' : 'form-control'}),
            'ruang' : forms.TextInput(attrs={'class' : 'form-control'}),
            'deskripsi' : forms.Textarea(attrs={'class' : 'form-control', 'rows':4}),
        }
